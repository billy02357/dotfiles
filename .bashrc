case $- in
*i*) ;;
*) return;;
esac

# ------------------------------------- env --------------------------------------------
export GITUSER="$USER"
export DOTFILES="$HOME/repos/$GITUSER/dotfiles"
export SCRIPTS="$HOME/repos/$GITUSER/scripts"
export REPOS="$HOME/repos/$GITUSER"
export DIRCOLORS="$HOME/.config/dircolors/dircolors"
export GOPATH="/usr/local/go"
export LYNX_CFG="$HOME/.config/lynx/lynx.cfg"
export TERM=xterm-256color
export HRULEWIDTH=73
export EDITOR=vi
export VISUAL=vi
export EDITOR_PREFIX=vi
export PS1

# ------------------------------------- pager ------------------------------------------
if test -x /usr/bin/lesspipe; then
        export LESSOPEN="| /usr/bin/lesspipe %s";
        export LESSCLOSE="/usr/bin/lesspipe %s";
fi

# ----------------------------------- history ------------------------------------------
export HISTCONTROL=ignoreboth
export HISTSIZE=5000
export HISTFILESIZE=10000
set -o vi
shopt -s histappend

# ------------------------------------ path --------------------------------------------
export PATH="$SCRIPTS:$HOME/.local/bin:/usr/local/bin:$GOPATH/bin:/bin:/sbin:/usr/bin:$HOME/.local/bin/tetrio"

# ---------------------------------- dircolor ------------------------------------------
if command -v dircolors &>/dev/null; then
        if test -r ~/.config/dircolors/dircolors; then
                eval "$(dircolors -b ~/.config/dircolors/dircolors)"
        else
                eval "$(dircolors -b)"
        fi
fi

# ----------------------------------- colors -------------------------------------------
export CEND='\[\e[0m\]'
export CBLUE='\[\e[34m\]'
export CYELLOW='\[\e[33m\]'
export CRED='\[\e[31m\]'
export CLGREY='\[\e[37m\]'
export CGREEN='\[\e[36m\]'

# ----------------------------------- prompt -------------------------------------------
ahead() {
        if test "$(git status -sb | grep "ahead")"; then
                printf "$CGREEN[ahead "; printf "%s" "$(git status | grep commit | head -n 1 | awk '{print $8}')"; printf "]$CEND"
        fi
}

__ps1() {
        if test -d .git; then
            PS1="\[\e[7m%\e[m$( printf "%*s" "$((COLUMNS-1))" "" )\r\e[K\]$CYELLOW╔ \u$CLGREY@\h:$CBLUE\W$CEND$CRED($(git branch | awk '{print $2}'))$CEND$(ahead)\n$CYELLOW╚ \$$CEND "
        else
            PS1="\[\e[7m%\e[m$( printf "%*s" "$((COLUMNS-1))" "" )\r\e[K\]$CYELLOW╔ \u$CLGREY@\h:$CBLUE\W$CLGREY$CEND\n$CYELLOW╚ \$$CEND "
        fi
}

PROMPT_COMMAND='__ps1'

# ---------------------------------- CDPATH -------------------------------------------
export CDPATH=./:\
~/repos/:\
~/repos/$GITUSER:\
~

# --------------------------------- Functions -----------------------------------------

cdd() {
        cd "$@" &>/dev/null
}

cdtemp() {
        cd "$(mktemp -d)" || exit 1
}

vitemp() {
        vi "$(mktemp)" || exit 1
}

x() {
        exit
}

# -------------------------------- Aliases --------------------------------------------
alias '?'='ddg'
alias '??'='brv'
alias t='touch'
alias cd='cdd'
alias dot='cd $DOTFILES'
alias cl='printf "\e[H\e[2J"'
alias g++='g++ -ansi -pedantic -Wall -Wextra -Weffc++ -std=c++11'
alias ls='/bin/ls -lh --color=auto'
alias sl='ls'
alias hibernate='systemctl hibernate'
alias vi='vim'

date +%d/%m/%Y | fdate
