set nocompatible
set rnu
set nu
set expandtab     "Tabs to spaces
set tabstop=4
set shiftwidth=4
set hlsearch      "Highlight searches
set incsearch     " Highlight while writing search
set hidden
set wildmenu      "Command line completion
set ruler
set confirm
set pastetoggle=<F11>
set aw
set showmode
set showcmd

set t_Co=256

au FileType go setl noet " Don't convert tabs to spaces when filetype is Golang source file (Go standard uses tabs)

set cmdheight=2

" Maps
nnoremap <CR> :noh<CR>

" Saving my muscle memory in 8 lines
nnoremap <right> :echoerr "Use l instead"<CR>
nnoremap <left> :echoerr "Use h instead"<CR>
nnoremap <up> :echoerr "Use k instead"<CR>
nnoremap <down> :echoerr "Use j instead"<CR>
inoremap <right> <NOP>
inoremap <left> <NOP>
inoremap <up> <NOP>
inoremap <down> <NOP>

if has('syntax')
  syntax on
endif

let $PAGER=''
let $LANG='en'

let @s = "ggI#!/bin/sh\<Esc>o\<Esc>omain() {\<Esc>o\<Esc>o}\<Esc>o\<Esc>omain \"$@\"\<Esc>"
let @c = "ggI#include <stdio.h>\<Esc>o\<Esc>oint main(int argc, char *argv[]) {\<Esc>o        printf(\"Hello world\");\<Esc>o\<Esc>o        return 0;\<Esc>o}\<Esc>"

map Y 0y$

call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'
Plug 'vim-syntastic/syntastic'
Plug 'bfrg/vim-cpp-modern'
Plug 'fatih/vim-go'

call plug#end()

let g:gruvbox_contrast_dark='medium'
let g:gruvbox_improved_warnings='1'
let g:gruvbox_improved_strings='1'

colorscheme gruvbox
" color torte
" color slate
" color ron
" color koehler
" color industry
" color elflord
" color default
set background=dark
